var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mysql      = require('mysql');

const connection = mysql.createPool({
  host     : '10.242.73.67',
  user     : 'root',
  password : 'root',
  database : 'timesheet'
});

app.use(express.static('public'));
app.get('/', function (req, res) {
   res.sendFile( __dirname + "/" + "index.html" );
})

app.get('/background.png', function (req, res) {
    res.sendFile( __dirname + "/" + "background.png" );
 })

 app.get('/timesheet-icon.ico', function (req, res) {
   res.sendFile( __dirname + "/" + "/timesheet-icon.ico" );
})

app.get('/bootstrap.min.css', function (req, res) {
    res.sendFile( __dirname + "/" + "bootstrap.min.css" );
 })

 app.get('/style.css', function (req, res) {
   res.sendFile( __dirname + "/" + "style.css" );
})

 app.get('/bootstrap.min.js', function (req, res) {
    res.sendFile( __dirname + "/" + "bootstrap.min.js" );
 })

 app.get('/jquery.min.js', function (req, res) {
    res.sendFile( __dirname + "/" + "jquery.min.js" );
 })

 app.get('/jquery.datepick.css', function (req, res) {
    res.sendFile( __dirname + "/" + "jquery.datepick.css" );
 })

app.get('/jquery.plugin.min.js', function (req, res) {
    res.sendFile( __dirname + "/" + "jquery.plugin.min.js" );
 })

 app.get('/jquery.datepick.js', function (req, res) {
    res.sendFile( __dirname + "/" + "jquery.datepick.js" );
 })

 app.get('/app.js', function (req, res) {
    res.sendFile( __dirname + "/" + "app.js" );
 })
 app.get('/task', function (req, res) {
    res.sendFile( __dirname + "/" + "task-list.html" );
 })
 
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())

//connection.connect();    

app.get('/codes/:code', function (req, res) {
    connection.query("SELECT * from code_master where code='"+req.params.code+"' order by name", function (error, results, fields) {
      if (error) throw error;
        res.send(results)
      });
})

app.get('/job-code-list', function (req, res) {
    connection.query("SELECT * from job_code_details where status='ACTIVE' order by project_name ", function (error, results, fields) {
      if (error) throw error;
      res.send(results)
      });
})

app.get('/task-list', function (req, res) {
    
    connection.query("SELECT id,emp_id,DATE_FORMAT(reporting_date, '%d-%m-%Y') as reporting_date,reporting_date as r_date,job_code,project_name,task_accomplished,hours_spend,activity,reusable,reusable_component,industry,comments FROM daily_time_sheet order by r_date desc", function (error, results, fields) {
      if (error) throw error;
      res.send(results)
      });
})

app.post('/timesheet',function(req,res){
    
    var sql = "INSERT INTO daily_time_sheet SET ?"
    var values = [req.body.emp_id,req.body.reporting_date,req.body.job_code,req.body.project_name,
                req.body.task_accomplised,req.body.hours_spend,req.body.activity,req.body.other_activity,req.body.reusable,req.body.reusable_component,req.body.other_reusable_component,req.body.industry,req.body.comments]
    connection.query(sql,req.body, function (error, results, fields) {
      if (error) throw error;
      console.log("error",error)
      res.send('Completed')
      });
     
})

app.get('/latest-task/:emp_id', function (req, res) {
   connection.query("SELECT * from daily_time_sheet where emp_id='"+req.params.emp_id+"' order by reporting_date desc limit 1", function (error, results, fields) {
     if (error) throw error;
       res.send(results)
     });
})

app.get('/tasks/:emp_id', function (req, res) {
   connection.query("SELECT * from daily_time_sheet where emp_id='"+req.params.emp_id+"' order by reporting_date desc", function (error, results, fields) {
     if (error) throw error;
       res.send(results)
     });
})
   
var server = app.listen(8082, function () {
   var host = server.address().address
   var port = server.address().port
   console.log("Example app listening at http://%s:%s", host, port)
})


