$(document).ready(function () {
  $("#errorMsg").hide();
  $("#dataLoader").hide();
  $("#tableDiv").hide();
  
  $("#emptytableDiv").show();
  $("#my-link-1").click(function(){
    window.location.href = "/";
  });

  $("#my-link-2").click(function(){
    window.location.href = "/task";
  });
  
  $("#search").on("keyup", function(e) {
    var value = $(this).val().toLowerCase();
    if(e.keyCode==13){
     if(value){
      $("#dataLoader").show();
      $("#emptytableDiv").show();
    $.getJSON("/tasks"+"/"+value, function (data) {
      $("#dataLoader").hide();
      $("#tableDiv").show();
      $("#emptytableDiv").hide();
      var number_of_rows = data.length;
      var table_body = '<table border="1" ><thead style="background-color:#bdc3c7;color:#000"><tr><th width="10%">Employee Id</th><th width="10%">Date</th><th width="20%">Activity</th><th width="16%">Project Name</th><th width="9%">Hour Spend</th><th width="35%">Task Accomplished</th></tr></thead><tbody>';
      
      for(i =0;i<data.length;i++){
            table_body+='<tr>';
    
            table_body +='<td>';
            table_body +=data[i]["emp_id"];
            table_body +='</td>';
    
            table_body +='<td>';
            table_body +=data[i]["reporting_date"];
            table_body +='</td>';
           
            table_body +='<td>';
            table_body +=data[i]["activity"];
            table_body +='</td>';
    
            table_body +='<td>';
            table_body +=data[i]["project_name"];
            table_body +='</td>';
        
            table_body +='<td>';
            table_body +=data[i]["hours_spend"];
            table_body +='</td>';
            
           table_body +='<td>';
           table_body +=data[i]["task_accomplished"];
           table_body +='</td>';
    
           table_body+='</tr>';
      }
    
        table_body+='</tbody></table>';
          $('#tableDiv').html(table_body);
        });
        $("#errorMsg").hide();
    }else{
      $("#errorMsg").show();
      $("#tableDiv").hide();
      $("#emptytableDiv").show();
    }
  }
    
});

      let employeeList = $('#employeId');
      $.getJSON("/codes/EMPID", function (data) {
        employeeList.append($('<option value="default" selected="selected">Employee Id</option>'));
        $.each(data, function (key, entry) {
          employeeList.append($('<option></option>').attr('value', entry.name).text(entry.name));
        })
      });

      $('#employeId').change(function(){
        $.getJSON("/latest-task"+"/"+this.value, function (data) {
        $("#jobCode").val(data[0].job_code);
        $("#projectName").val(data[0].project_name);
        $("#datepicker").val(data[0].reporting_date);
        $("#taskAccomplished").val(data[0].task_accomplished);
        $("#hourSpent").val(data[0].hours_spend);
        $("#activityID").val(data[0].activity);
        var otherActivity = $("#activityID").val();
        if(otherActivity=="Others"){
          $("#othersValueActivity").show();
        }else{
          $("#othersValueActivity").hide();
        }
        $("#othersValueActivity").val(data[0].other_activity);
        if(data[0].reusable=="No"){
          $("#reusableComponent").hide();
          $('#reusableNo').prop('checked',true);
        }else{
          $("#reusableComponent").show();
          $('#reusableYes').prop('checked',true);
          $("#reusableComponent").val(data[0].reusable_component);
        }
        $("#industry").val(data[0].industry);
        $("#comment").val(data[0].comments);
        });
      });

      let jobCodeList = $('#jobCode');
      let projectNameList=$("#projectName")
      $.getJSON("/job-code-list", function (data) {
        jobCodeList.append($('<option value="default" selected="selected">Job Code</option>'));
        projectNameList.append($('<option value="default" selected="selected">Project Name</option>'));
        $.each(data, function (key, entry) {
          jobCodeList.append($('<option></option>').attr('value', entry.job_code).text(entry.job_code));
          projectNameList.append($('<option></option>').attr('value', entry.project_name).text(entry.project_name));
        })
      });

    $("#reusableNo").click(function(){
      $("#reusableComponent").hide();
      // $("#othersValueReusable").hide();
    });

    $("#reusableYes").click(function(){
      $("#reusableComponent").show();
      // $("#othersValueReusable").show();
    });

    $("#othersValueActivity").hide();
    $("#activityID").click(function(){
     
      if($("#activityID").val()=="Others"){
        $("#othersValueActivity").show();
      }else{
        $("#othersValueActivity").hide();
      }
    });

    $("#othersValueReusable").hide();
    $("#reusableComponent").click(function(){
     
      if($("#reusableComponent").val()=="Others"){
        $("#othersValueReusable").show();
      }else{
        $("#othersValueReusable").hide();
        
      }
    });

  $("#reusableYes").prop("checked", true);

    $(function() {
        $("#datepicker").datepick({ dateFormat: 'yyyy-mm-dd' });
    });
    
    $("#submitForm").click(function(){
            var employeIdValue =  $("#employeId").val();
            var selectedDateValue=$("#datepicker").val();
            var activityIDValue = $("#activityID").val();
            var otherActivityValue=$("#othersValueActivity").val();
            var jobCodeValue = $("#jobCode").val();
            var projectNameValue= $("#projectName option:selected").val();
            var taskAccomplishedValue=$("#taskAccomplished").val();
            var hourSpentValue=$("#hourSpent").val();
            var reusableComponentValue=$("#reusableComponent").val();
            var otherReusableComponentValue=$("#othersValueReusable").val();
            var industryValue = $("#industry").val();
            var commentValue=$("#comment").val();
            var reusableRdValue=$("input[name='reusableRD']:checked").val();
            if(reusableRdValue=="No"){
              reusableComponentValue="NA";
            }else{
              reusableComponentValue;
            }
            
            if(activityIDValue!="Others"){
              otherActivityValue="";
            }else{
              otherActivityValue;
            }
            // console.log("otherActivityValue---->",otherActivityValue)
            // console.log("employeIdValue---->",employeIdValue,"selectedDateValue--->",selectedDateValue,"activityIDValue--->",activityIDValue,"jobCodeValue--->",jobCodeValue,"projectNameValue--->",projectNameValue,"taskAccomplishedValue--->",taskAccomplishedValue,"hourSpentValue--->",hourSpentValue,"reusableComponentValue--->",reusableComponentValue,"industryValue--->",industryValue,"commentValue--->",commentValue)

            if(employeIdValue!='default' && selectedDateValue!='' && activityIDValue!='default' && jobCodeValue!='default' && projectNameValue!='default' && taskAccomplishedValue!='' && hourSpentValue!='' && reusableComponentValue!='default' && industryValue!='default' &&  commentValue!=''){
               $.post('/timesheet',   
            {"emp_id":employeIdValue,"reporting_date":selectedDateValue,"job_code":jobCodeValue,"project_name":projectNameValue,"task_accomplished":taskAccomplishedValue,"hours_spend":hourSpentValue,"activity":activityIDValue,"other_activity":otherActivityValue,"reusable":reusableRdValue,"reusable_component":reusableComponentValue,"other_reusable_component":otherReusableComponentValue,"industry":industryValue,"comments":commentValue}, 
            function(data, status) {
                    alert("SUCCESSFULL");
                    location.reload();
             });
            }else{
              alert("FILL ALL THE FIELDS")
            }
            
      });
});